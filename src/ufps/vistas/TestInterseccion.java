/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author Harold
 */
public class TestInterseccion {
    public static void main (String[]Args){
        ListaS<String> instance = new ListaS();
        ListaS<String> instance2 = new ListaS();
        
        instance.insertarAlInicio("Marco");
        instance.insertarAlInicio("Maria");
        instance.insertarAlInicio("Reynaldo");
        instance.insertarAlInicio("Carlos");
        instance.insertarAlInicio("Carlos");
        
        instance2.insertarAlInicio("Madarme");
        instance2.insertarAlInicio("Carlos");
        instance2.insertarAlInicio("Reynaldo");
        instance2.insertarAlInicio("Fabiola");
        instance2.insertarAlInicio("Grant");
        
        ListaS<Integer> interseccion = instance.interseccion(instance2);
        System.out.println("La intersección entre las listas es: "+interseccion.toString());
    }
}
