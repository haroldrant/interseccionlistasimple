/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author MADARME
 */
public class ListaSIT {
    
    public ListaSIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of insertarAlInicio method, of class ListaS.
     */
    @Test
    public void testInsertarAlInicio() {
        System.out.println("insertarAlInicio");
      
        ListaS<Integer> instance = new ListaS();
        instance.insertarAlInicio(1);
        // TODO review the generated test code and remove the default call to fail.
        instance.insertarAlInicio(2);
        boolean p1=instance.get(0)==2;
        boolean p2=instance.get(1)==1;
        
        
        assertTrue(p1);
        assertTrue(p2);
       
    }

    /**
     * Test of testsortSelection method, of class ListaS.
     */
    
     @Test
    public void testsortSelection() {
        System.out.println("sortSelection");
      
        ListaS<Integer> instance = new ListaS();
        instance.insertarAlInicio(1);
        instance.insertarAlInicio(3);
        instance.insertarAlInicio(0);
        // TODO review the generated test code and remove the default call to fail.
        
        ListaS<Integer> instance2 = new ListaS();
        instance2.insertarAlFinal(0);
        instance2.insertarAlFinal(1);
        instance2.insertarAlFinal(3);
        
        //Invoco el método a probar:
        instance.sortSelection();
        
        
        //Invoco la prueba con la lista de prueba instance2:
        //para esto es necesario tener la equals implementado en la clase Lista Simple
        assertEquals(instance, instance2);
        //fail("No paso la prueba de sortSelection");
       
    }
    
    
}
