/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.util.Iterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Harold
 */
public class InterseccionTest {
    
    public InterseccionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of interseccion method, of class ListaS.
     */
    @Test
    public void testInterseccion() {
        ListaS instance2 = new ListaS();
        ListaS instance = new ListaS();
        ListaS esperado = new ListaS();
        
        instance.insertarAlInicio(2);
        instance.insertarAlInicio(7);
        instance.insertarAlInicio(8);
        instance.insertarAlInicio(5);
        instance.insertarAlInicio(18);
        
        instance2.insertarAlInicio(28);
        instance2.insertarAlInicio(8);
        instance2.insertarAlInicio(6);
        instance2.insertarAlInicio(2);
        instance2.insertarAlInicio(18);
        
        esperado.insertarAlFinal(2);
        esperado.insertarAlFinal(8);
        esperado.insertarAlFinal(18);
        
        ListaS result = instance.interseccion(instance2);
        assertEquals(esperado, result);
        
    }    
}
